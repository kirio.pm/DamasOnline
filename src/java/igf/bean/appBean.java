package igf.bean;

import igf.util.MyUtil;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

@ManagedBean
@ApplicationScoped
public class appBean {

    public appBean() {
    }
    
    public String getBaseUrl(){
        return MyUtil.baseurl();
    }
    
    public String getBasePath(){
        return MyUtil.basepath();
    }
   
}
