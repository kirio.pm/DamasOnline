package igf.bean;

import igf.dao.UsuarioDao;
import igf.dao.UsuarioDaoImpl;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import igf.model.Usuario;
import org.primefaces.context.RequestContext;
import igf.util.MyUtil;

@ManagedBean(name ="loginBean")
@SessionScoped
public class loginBean implements Serializable{

    private String username;
    private String password;
    private UsuarioDao usuarioDao;
    
    public loginBean() {
        this.usuarioDao = new UsuarioDaoImpl();
    }
   
    public String getUsername(){
        return username;
    }
    
    public String getPassword(){
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void login() {  
        RequestContext context = RequestContext.getCurrentInstance();  
        FacesMessage msg;  
        boolean loggedIn; 
        String ruta = "";
       Usuario modelo = usuarioDao.findByUsuario(username,password);
        if(modelo != null) {  
            loggedIn = true; 
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", username);
            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", username);
            ruta = MyUtil.basepathlogin()+"vistas/home.xhtml";
        } else {  
            loggedIn = false;  
            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Error", "Usuario y/o Clave es incorrecto.");
        }  

        FacesContext.getCurrentInstance().addMessage(null, msg);  
        context.addCallbackParam("loggedIn", loggedIn);  
        context.addCallbackParam("ruta", ruta);
    }

    public void logout(){
        String ruta = MyUtil.basepathlogin()+"login.xhtml";
        RequestContext context = RequestContext.getCurrentInstance();
        FacesContext facesContext = FacesContext.getCurrentInstance();

        HttpSession sesion = (HttpSession) facesContext.getExternalContext().getSession(false);
        sesion.invalidate();

        context.addCallbackParam("loggetOut", true);
        context.addCallbackParam("ruta", ruta);
    }
}
