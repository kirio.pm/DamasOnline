package igf.bean;

import igf.dao.UsuarioDao;
import igf.dao.UsuarioDaoImpl;
import igf.model.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public class usuarioBean implements Serializable {

    private List<Usuario> listar;
    private List<SelectItem> listaRoles;

    
    private Usuario  usuario;
    public usuarioBean() {
        usuario=new Usuario();
    }
    
    public List<Usuario> getListar() {
        UsuarioDao eUsuario = new UsuarioDaoImpl();
        listar=eUsuario.listaDeUsuario();
        return listar;
    }
    
    public void nuevoUsuario(){
         UsuarioDao nUsuario = new UsuarioDaoImpl();
         nUsuario.nuevoUsuario(usuario);
         usuario=new Usuario();
    }
    public void modificarUsuario(){
         UsuarioDao nUsuario = new UsuarioDaoImpl();
         nUsuario.actualizarUsuario(usuario);
         usuario=new Usuario();
    }
    public void suprimirUsuario(){
         UsuarioDao nUsuario = new UsuarioDaoImpl();
         nUsuario.eliminarUsuario(usuario);
         usuario=new Usuario();
    }
    
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    
}
