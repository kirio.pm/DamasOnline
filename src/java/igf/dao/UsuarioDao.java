/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package igf.dao;

import igf.model.Rol;
import igf.model.Usuario;
import java.util.List;

/**
 *
 * @author kirio
 */
public interface UsuarioDao {
    public List<Usuario> listaDeUsuario();
    public void nuevoUsuario(Usuario usuario);
    public void actualizarUsuario(Usuario usuario);
    public void eliminarUsuario(Usuario usuario);
    public List<Rol> listaRoles();
    public Usuario findByUsuario(String username, String password);
    public Usuario login(Usuario usuario);
    
}
