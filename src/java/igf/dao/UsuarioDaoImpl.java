/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package igf.dao;

import igf.model.Rol;
import igf.model.Usuario;
import igf.util.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author kirio
 */
public class UsuarioDaoImpl implements UsuarioDao {

    @Override
    public List<Usuario> listaDeUsuario() {
  List<Usuario> ls=null;
  Session session=HibernateUtil.getSessionFactory().openSession();
  Transaction transaction=session.beginTransaction();
  String hql="FROM Usuario u left join fetch u.rol";
   try {
            ls =  session.createQuery(hql).list();
            transaction.commit();
            session.close();
        } catch (Exception e) {
            transaction.rollback();
        }
        return ls;
  
    }

    @Override
    public void nuevoUsuario(Usuario usuario) {
        Session session=null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(usuario);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            
        }finally{
        if(session !=null){
            session.close();
        }
        }
    }
    @Override
    public void actualizarUsuario(Usuario usuario) {
       Session session=null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.update(usuario);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            
        }finally{
        if(session !=null){
            session.close();
        }
        }
    }

    @Override
    public void eliminarUsuario(Usuario usuario) {
        Session session=null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.delete(usuario);
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            
        }finally{
        if(session !=null){
            session.close();
        }
        }
       
    }
    @Override
    public List<Rol> listaRoles() {
    List<Rol> lr=null;
    Session session=HibernateUtil.getSessionFactory().openSession();
    Transaction transaction=session.beginTransaction();
    String hql="FROM Rol";
    try {
            lr =  session.createQuery(hql).list();
            transaction.commit();
            session.close();
        } catch (Exception e) {
            transaction.rollback();
        }
        return lr;
       
    }

     @Override
    public Usuario findByUsuario(String username, String password) {
         Session session = HibernateUtil.getSessionFactory().getCurrentSession();
         session.beginTransaction();
        Criteria criteria = session.createCriteria(Usuario.class);
        criteria.add(Restrictions.eq("usuario", username));
        //criteria.add(Restrictions.eq("clave", password));
         return (Usuario)criteria.uniqueResult();
    }

    @Override
    public Usuario login(Usuario usuario) {
        return null;
    }

    
    
}
