package moduloJuego.juegoDamas;

public class Pieza {

	private String posicionX; // valores letras
	private String posicionY; // valores numeros
	private String parteDelTablero;
	private Movimiento manejadorMovimiento;

	public Pieza(String posicionX, String posicionY, Jugador ladoJugador) {
		this.posicionX = posicionX;
		this.posicionY = posicionY;
		this.parteDelTablero = ladoJugador.toString();
	}

	public void initialize(Movimiento manejador) {
		this.manejadorMovimiento = manejador;
	}

	public void actualizarPosicion(String posicionX, String posicionY) {
		this.posicionX = posicionX;
		this.posicionY = posicionY;
	}

	public void mover(String posicionX, String posicionY) {
		this.posicionX = posicionX;
		this.posicionY = posicionY;
	}

	public String getParteDelTablero() {
		return parteDelTablero;
	}

	public String getPosicionX() {
		return posicionX;
	}

	public void setPosicionX(String posicionX) {
		this.posicionX = posicionX;
	}

	public String getPosicionY() {
		return posicionY;
	}

	public void setPosicionY(String posicionY) {
		this.posicionY = posicionY;
	}

	public Movimiento getManejadorMovimiento() {
		return manejadorMovimiento;
	}

	public void setManejadorMovimiento(Movimiento manejador) {
		this.manejadorMovimiento = manejador;
	}

	public int getPosicionYcomoEntero() {
		return Integer.parseInt(posicionY);
	}

	public int getPosicionXcomoEntero() {
		char x = posicionX.charAt(0);
		return (int) x;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((posicionX == null) ? 0 : posicionX.hashCode());
		result = prime * result + ((posicionY == null) ? 0 : posicionY.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pieza other = (Pieza) obj;
		if (posicionX == null) {
			if (other.posicionX != null)
				return false;
		} else if (!posicionX.equals(other.posicionX))
			return false;
		if (posicionY == null) {
			if (other.posicionY != null)
				return false;
		} else if (!posicionY.equals(other.posicionY))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Pieza [posicionX=" + posicionX + ", posicionY=" + posicionY + ", parteDelTablero=" + parteDelTablero
				+ "]";
	}

}
