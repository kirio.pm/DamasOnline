package moduloJuego.juegoDamas;

public interface Movimiento {
	
	public void mover(String posicionX, String posicionY);
	
	public void comer(String posicionX, String posicionY);
}
