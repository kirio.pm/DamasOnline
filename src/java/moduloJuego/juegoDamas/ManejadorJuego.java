package moduloJuego.juegoDamas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ManejadorJuego {
	
	private List<Pieza> piezasDelJuego;
	private List<String> nombreColumnasX = new ArrayList<String>();
	private List<String> nombreColumnasY = new ArrayList<String>();
	
	public ManejadorJuego() {
		inicializarNombreDeColumnasX();
		inicializarNombreDeColumnasY();
	}

	public void moverPieza(String x_actual, String y_actual, String x_destino, String y_destino) {
		Pieza pieza = getPieza(x_actual, y_actual);
		if(pieza != null) {
			if(esMovimientoParaComerPieza(pieza, x_destino, y_destino)) {
				pieza.getManejadorMovimiento().comer(x_destino, y_destino);
				removerPiezaDelTablero(calcularPosicionIntermediaX(x_actual, x_destino),
						calcularPosicionIntermediaX(y_actual, y_destino));
			}else {
				pieza.getManejadorMovimiento().mover(x_destino, y_destino);
			}
		}
	}
	
	public void iniciarNuevoJuego() {
		piezasDelJuego = new ArrayList<Pieza>();
		for(int posicionY = 1; posicionY < 4; posicionY ++) {
			crearPiezasNorte(posicionY);
		}
		for(int posicionY = 6; posicionY < 9; posicionY ++) {
			crearPiezasSur(posicionY);
		}
	}
	
	public void crearPiezasNorte(int posicionY) {
		for(int posicionX = 1; posicionX < 9; posicionX++) {
			if(esCampoValido(posicionX, posicionY)) {
				Pieza pieza = new Pieza(nombreColumnasX.get(posicionX),
						nombreColumnasY.get(posicionY), Jugador.NORTE);
				Movimiento manejador = new MovimientoFicha(pieza, this);
				pieza.initialize(manejador);
				piezasDelJuego.add(pieza);
			}
		}
	}
	
	//TODO CÓDIGO REPETIDO
	public void crearPiezasSur(int posicionY) {
		for(int posicionX = 1; posicionX < 9; posicionX++) {
			if(esCampoValido(posicionX, posicionY)) {
				Pieza pieza = new Pieza(nombreColumnasX.get(posicionX),
						nombreColumnasY.get(posicionY), Jugador.SUR);
				Movimiento manejador = new MovimientoFicha(pieza, this);
				pieza.initialize(manejador);
				piezasDelJuego.add(pieza);
			}
		}
	}
	
	public Pieza getPieza(String posicionX,String  posicionY) {
		for(Pieza pieza : piezasDelJuego) {
			if(pieza.getPosicionX().equalsIgnoreCase(posicionX) &&
					pieza.getPosicionY().equalsIgnoreCase(posicionY)) {
				return pieza;
			}
		}
		return null;
	}
	
	public boolean esMovimientoParaComerPieza(Pieza pieza, String x_destino, String y_destino) {
		String posicionIntermediaX = calcularPosicionIntermediaX(pieza.getPosicionX(), x_destino);
		String posicionIntermediaY = calcularPosicionIntermediaY(pieza.getPosicionY(), y_destino);
		Pieza piezaIntermedia = getPieza(posicionIntermediaX, posicionIntermediaY);
		return (piezaIntermedia != null && !(pieza.getParteDelTablero().equalsIgnoreCase(piezaIntermedia.getParteDelTablero())));
	}
	
	public List<Pieza> getCamposDelTablero(){
		return piezasDelJuego;
	}
	
	public void removerPiezaDelTablero(String posicionX, String posicionY) {
		for(Iterator<Pieza> iterator = piezasDelJuego.iterator(); iterator.hasNext();) {
			Pieza pieza = iterator.next();
			if(pieza.getPosicionX().equalsIgnoreCase(posicionX) && pieza.getPosicionY().equalsIgnoreCase(posicionY)) {
				iterator.remove();
			}
		}
	}
	
	public boolean esCampoValido(int x, int y) {
		return (((x-y)%2)==0);
	}
	
	public String calcularPosicionIntermediaX(String p1, String p2) {
		return Character.toString((char)(((int)p1.charAt(0) + (int)p2.charAt(0))/2));
	}
	
	public String calcularPosicionIntermediaY(String p1, String p2) {
		return String.valueOf((Integer.parseInt(p1) + Integer.parseInt(p2))/2);
	}
	
	public void inicializarNombreDeColumnasX() {
		nombreColumnasX.add(0,"INVALID");
		nombreColumnasX.add(1,"A");
		nombreColumnasX.add(2,"B");
		nombreColumnasX.add(3,"C");
		nombreColumnasX.add(4,"D");
		nombreColumnasX.add(5,"E");
		nombreColumnasX.add(6,"F");
		nombreColumnasX.add(7,"G");
		nombreColumnasX.add(8,"H");
	}
	
	public void inicializarNombreDeColumnasY() {
		nombreColumnasY.add(0,"INVALID");
		nombreColumnasY.add(1,"1");
		nombreColumnasY.add(2,"2");
		nombreColumnasY.add(3,"3");
		nombreColumnasY.add(4,"4");
		nombreColumnasY.add(5,"5");
		nombreColumnasY.add(6,"6");
		nombreColumnasY.add(7,"7");
		nombreColumnasY.add(8,"8");
	}


}
