package moduloJuego.juegoDamas;

public class MovimientoFicha implements Movimiento{
	
	private Pieza pieza;
	private ManejadorJuego manejadorJuego;
	private boolean esPermitidoMover;
	
	public MovimientoFicha(Pieza pieza, ManejadorJuego manejador) {
		this.pieza = pieza;
		this.manejadorJuego = manejador;
		esPermitidoMover = true;
	}

	@Override
	public void mover(String posicionX, String posicionY) {
		resetearPermisos();
		verificarMovimientoDentroDelLimite(posicionX, posicionY);
		verificarMovimientoAlFrenteDelJugador(posicionY);
		verificarCampoVacio(posicionX, posicionY);
		if(esPermitidoMover) {
			pieza.actualizarPosicion(posicionX, posicionY);
		}
	}
	
	@Override
	public void comer(String posicionX, String posicionY) {
		resetearPermisos();
		verificarMovimientoAlFrenteDelJugador(posicionY);
		if(esPermitidoMover) {
			pieza.actualizarPosicion(posicionX, posicionY);
		}
	}
	
	public void verificarMovimientoDentroDelLimite(String posicionX, String posicionY) {
		if(noRangoXvalido(posicionX) && noRangoYvalido(posicionY)) {
			esPermitidoMover = false;
		}
	}
	
	public void verificarMovimientoAlFrenteDelJugador(String posicionY) {
		if(pieza.getParteDelTablero().equalsIgnoreCase("NORTE")) {
			verificarMovimientoAlfrenteJugadorNorte(posicionY);
		}else if(pieza.getParteDelTablero().equalsIgnoreCase("SUR")) {
			verificarMovimientoAlfrenteJugadorSur(posicionY);
		}
	}
	
	public void verificarCampoVacio(String posicionX, String posicionY) {
		if(manejadorJuego.getPieza(posicionX, posicionY) != null) {
			esPermitidoMover = false;
		}
	}
	
	public void verificarMovimientoAlfrenteJugadorNorte(String posicionY) {
		 if(posicionYnoValidaParaJugadorNorte(posicionY)) {
			 esPermitidoMover = false;
		 }
	}
	
	public void verificarMovimientoAlfrenteJugadorSur(String posicionY) {
		if(posicionYnoValidaParaJugadorSur(posicionY)) {
			esPermitidoMover = false;
		}
	}
	
	public void resetearPermisos() {
		esPermitidoMover = true;
	}
	
	public boolean noRangoYvalido(String posicionY) {
		return !((pieza.getPosicionYcomoEntero() + 1) == (Integer.parseInt(posicionY))||
				(pieza.getPosicionYcomoEntero() - 1) == (Integer.parseInt(posicionY)));
	}
	
	public boolean noRangoXvalido(String posicionX) {
		return !((pieza.getPosicionXcomoEntero() + 1) == (posicionX.charAt(0))||
				(pieza.getPosicionXcomoEntero() - 1) == (posicionX.charAt(0)));
	}
	
	public boolean posicionYnoValidaParaJugadorSur(String posicionY) {
		return !(pieza.getPosicionYcomoEntero() >= Integer.parseInt(posicionY));
	}
	
	public boolean posicionYnoValidaParaJugadorNorte(String posicionY) {
		return !(pieza.getPosicionYcomoEntero() <= Integer.parseInt(posicionY));
	}

	
}
