package moduloJuego.test;


import junit.framework.TestCase;
import junit.textui.TestRunner;
import moduloJuego.juegoDamas.ManejadorJuego;
import moduloJuego.juegoDamas.Pieza;

public class TestManejadorJuego extends TestCase{
	
	private ManejadorJuego manejador;
	
	public static void main(String[] args) {
		TestRunner.main(new String[] {"TestManejadorJuego"});
	}
	
	public TestManejadorJuego(String name) {
		super(name);
	}
	
	public void setUp() throws Exception{
		manejador = new ManejadorJuego();
	}
	
	public void testIniciarNuevoJuegoConTodasLasPiezas() throws Exception{
		manejador.iniciarNuevoJuego();
		assertEquals(24, manejador.getCamposDelTablero().size());
		
	}
	
	public void testMoverPieza() throws Exception{
		manejador.iniciarNuevoJuego();
		manejador.moverPieza("C", "3", "D", "4");
		assertNotNull(manejador.getPieza("D", "4"));
		assertNull(manejador.getPieza("C", "3"));
	}
	
	public void testGetPieza() throws Exception{
		manejador.iniciarNuevoJuego();
		assertNotNull(manejador.getPieza("A", "1"));
		assertNull(manejador.getPieza("E","5"));
	}
	
	public void testEsCampoValido() throws Exception{
		assertTrue(manejador.esCampoValido(1, 1));
		assertTrue(manejador.esCampoValido(8, 8));
		assertFalse(manejador.esCampoValido(1, 8));
		assertFalse(manejador.esCampoValido(5, 4));
		
	}
	
	public void testMovimientoParaComerFicha() throws Exception{
		manejador.iniciarNuevoJuego();
		Pieza piezaSur = manejador.getPieza("B", "6");
		Pieza piezaNorte = manejador.getPieza("E", "3");
		piezaSur.mover("C", "5");
		piezaNorte.mover("D", "4");
		assertTrue(manejador.esMovimientoParaComerPieza(piezaSur, "E", "3"));
		assertTrue(manejador.esMovimientoParaComerPieza(piezaNorte, "B", "6"));
		assertFalse(manejador.esMovimientoParaComerPieza(piezaNorte, "E", "5"));
		assertFalse(manejador.esMovimientoParaComerPieza(piezaSur, "B", "4"));
	}
	
	public void testRemoverPiezaDelTablero() throws Exception{
		manejador.iniciarNuevoJuego();
		manejador.removerPiezaDelTablero("C", "3");
		assertNull(manejador.getPieza("C", "3"));
	}
	
	

}
