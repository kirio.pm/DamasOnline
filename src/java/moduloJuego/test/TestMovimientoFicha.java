package moduloJuego.test;


import junit.framework.TestCase;
import junit.textui.TestRunner;
import moduloJuego.juegoDamas.ManejadorJuego;
import moduloJuego.juegoDamas.Movimiento;
import moduloJuego.juegoDamas.Pieza;

public class TestMovimientoFicha extends TestCase{
	private Pieza piezaNorte1;
	private Pieza piezaSur1;
	private Pieza piezaNorte2;
	private Movimiento movimientoNorte1;
	private Movimiento movimientoNorte2;
	private Movimiento movimientoSur;
	private static final String POSICION_INICIAL_NORTE_X = "E";
	private static final String POSICION_INICIAL_NORTE_Y = "3";
	private static final String POSICION_INICIAL_SUR_X = "D";
	private static final String POSICION_INICIAL_SUR_Y = "6";
	
	ManejadorJuego manejador;
	
	public static void main(String[] args) {
		TestRunner.main(new String[] {"TestMovimientoFicha"});
	}
	
	public TestMovimientoFicha(String name) {
		super(name);
	}
	
	public void setUp() throws Exception{
		manejador = new ManejadorJuego();
		manejador.iniciarNuevoJuego();
		piezaNorte1 = manejador.getPieza(POSICION_INICIAL_NORTE_X, POSICION_INICIAL_NORTE_Y);
		movimientoNorte1 = piezaNorte1.getManejadorMovimiento();
		piezaSur1 = manejador.getPieza(POSICION_INICIAL_SUR_X, POSICION_INICIAL_SUR_Y);
		movimientoSur = piezaSur1.getManejadorMovimiento();
		piezaNorte2 = manejador.getPieza("C","3");
		movimientoNorte2 = piezaNorte2.getManejadorMovimiento();
	}
	
	public void testMoverFichaParaCampoVacio() throws Exception{
		movimientoNorte1.mover("F","4");
		assertEquals("F", piezaNorte1.getPosicionX());
		assertEquals("4", piezaNorte1.getPosicionY());
	}
	
	public void testMoverFichaDosPosiciones() throws Exception{
		movimientoNorte1.mover("G", "5");
		assertEquals(POSICION_INICIAL_NORTE_X, piezaNorte1.getPosicionX());
		assertEquals(POSICION_INICIAL_NORTE_Y, piezaNorte1.getPosicionY());
	}
	
	public void testMoverFichaNorteEnRetroceso() throws Exception{
		piezaNorte1.setPosicionX("E");
		piezaNorte1.setPosicionY("5");
		movimientoNorte1.mover("D","4");
		assertEquals("E", piezaNorte1.getPosicionX());
		assertEquals("5", piezaNorte1.getPosicionY());
	}
	
	public void testMoverFichaSurEnRetroceso() throws Exception{
		movimientoSur.mover("C", "7");
		assertEquals(POSICION_INICIAL_SUR_X, piezaSur1.getPosicionX());
		assertEquals(POSICION_INICIAL_SUR_Y, piezaSur1.getPosicionY());
	}
	
	public void testMoverFichaCampoOcupado() throws Exception{
		movimientoNorte1.mover("D", "4");
		movimientoNorte2.mover("D", "4");
		assertEquals("C", piezaNorte2.getPosicionX());
		assertEquals("3", piezaNorte2.getPosicionY());
	}
	
	public void testComerFicha() throws Exception{
		movimientoNorte1.mover("D", "4");
		Movimiento movimientoSur2 = manejador.getPieza("B","6").getManejadorMovimiento();
		movimientoSur2.mover("C","5");
	}

}
