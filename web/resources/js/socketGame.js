WEB_SOCKET_SWF_LOCATION = "WebSocketMain.swf";
        // Set this to dump debug message from Flash to console.log:
WEB_SOCKET_DEBUG = true;

        // Everything below is the same as using standard WebSocket.
var portGameSocket;
var messageTypesList = ["VIEWER","BOARD","TURN","PERMISSION", "STATUS","PORT","ERROR"]; 
var playerType = "PLAYER";
var ws;


$(document).ready(function() {
    portGameSocket = getCookie("port");   
    init();

});

function init() {

            // Connect to Web Socket.
            // Change host/port here to your own Web Socket server.
            
            ws = new WebSocket("ws://localhost:" + portGameSocket);
            
            // Set event handlers.
            ws.onopen = function() {
                  
              };
              ws.onmessage = function(e) {
                  messageTyped = e.data.split("$$");
                  for(var index = 0; index < messageTyped.length; index++){
                      if(messageTyped[index].split(";")[0] == messageTypesList[0]){
                          $('#viewsCount').empty();
                          $('#viewsCount').append(messageTyped[index].split(";")[1]);
                      }else if(messageTyped[index].split(";")[0] == messageTypesList[1]){
                          piezas = messageTyped[index].split(";")[1];
                          updateBoard(messageTyped[index].split(";")[1]);
                      }else if(messageTyped[index].split(";")[0] == messageTypesList[2]){
                          $('#playerTurn').empty();
                          $('#playerTurn').append(messageTyped[index].split(";")[1]);
                      }else if(messageTyped[index].split(";")[0] == messageTypesList[3]){
                          if(messageTyped[index].split(";")[1]  == "PLAYER"){
                              playerType = "PLAYER";
                            }else if(messageTyped[index].split(";")[1]  == "OBSERVER"){
                                playerType = "OBSERVER";
                                $('#score').empty();
                                $('#score').append("Has ingresado como espectador");
                            }                 
                         }
                  }
                  if(playerType == "PLAYER"){
                      setBoartProperties();
                      addAction();
                  }
              };
              ws.onclose = function() {
                  
              };
              ws.onerror = function() {
                  
              };

          }

          function onSubmit() {
              var input = document.getElementById("input");
              // You can send message to the Web Socket using ws.send.
              ws.send(input.value);
              output("Enviando: " + input.value);
              input.value = "";
              input.focus();
          }

          function onCloseClick() {
              ws.close();
          }

          function output(str) {
              var log = document.getElementById("log");
              var escaped = str.replace(/&/, "&amp;").replace(/</, "&lt;").
              replace(/>/, "&gt;").replace(/"/, "&quot;"); // "
              log.innerHTML = escaped + "<br>" + log.innerHTML;
          }

        
        
        