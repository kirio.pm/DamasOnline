$(document).ready(function(){
    init(); 
});
// Set URL of your WebSocketMain.swf here:
        WEB_SOCKET_SWF_LOCATION = "WebSocketMain.swf";
        // Set this to dump debug message from Flash to console.log:
        WEB_SOCKET_DEBUG = true;

        // Everything below is the same as using standard WebSocket.

        var portMainSocket = "9999/";
        var portGameSocket = "";

        var ws;

        function init() {

            // Connect to Web Socket.
            // Change host/port here to your own Web Socket server.
            ws = new WebSocket("ws://localhost:" + portMainSocket);

            // Set event handlers.
            ws.onopen = function() {
                showSuccsessfullConection();
            };
            ws.onmessage = function(e) {
                action = e.data.split(";");
                if(action[0] == "GAMEPORT"){
                    portGameSocket = action[1];
                    checkCookie();  
                    
                }else if(action[0] == "PORTLIST"){
                    port_list = action[1].split(";");
                    $('#portList').append("<h3>Lista de puertos disponibles</h3>");
                    if(port_list.length > 0){
                        for(var index = 0; index < port_list.length; index++){
                            $('#portList ul').append("<li><a href='#' onclick='showGameInPort(" +port_list[index] + ")'>Partida " + index +"<i class='material-icons'>videogame_asset</i></a></li>");
                        }
                    }else{
                      $('#portList').append("<p>No se encontraron partidas disponibles</p>");
                    }
                }else{
                    alert("No se pudo obtener respuesta correcta del servidor");
                }
            };
            ws.onclose = function() {

            };
            ws.onerror = function() {
                showErrorConection();
            };

        }

        function onNewGame(){
            ws.send("NEWGAME");
        }

        function onShowGame(){
            ws.send("SHOW");
        }

        function showSuccsessfullConection(){
            $('#conectionStatus').append("check_circle");
        }

        function showErrorConection(){
            $('#conectionStatus').append("error");
            $('#errorInfo').append("No se pudo conectar con el servidor.");
        }
        
        function showGameInPort(port){
            portGameSocket = port;
            checkCookie();
           
        }


