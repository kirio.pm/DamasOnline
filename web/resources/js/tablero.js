$(document).ready(function() {
  var cantTr=9, cantTd=9, filas=['A','B','C','D','E','F','G','H'];
  var piezas = "A:1:NORTE,C:1:NORTE,E:1:NORTE,G:1:NORTE,B:2:NORTE,D:2:NORTE,F:2:NORTE,H:2:NORTE,A:3:NORTE,C:3:NORTE,E:3:NORTE,G:3:NORTE,B:6:SUR,D:6:SUR,F:6:SUR,H:6:SUR,A:7:SUR,C:7:SUR,E:7:SUR,G:7:SUR,B:8:SUR,D:8:SUR,F:8:SUR,H:8:SUR,";

  for (var row = 1; row < cantTr; row++) {
      $("#tablero").append("<tr id="+ row +"></tr>");
  }

  for(var index = 0; index < cantTr -1; index++){
     row = $('#tablero tr:eq(' + index +')');
     for(var column = 1; column < cantTd; column++){
       row.append('<td id=' + filas[column - 1] + (index + 1)  + '></td>');
       if(isValidPosition(column,index + 1)){
        $('#' + filas[column -1] + (index + 1)).addClass("sortable");
       }
     }
  }

    updateBoard(piezas);
    setBoartProperties();
    addAction();
});

function updateBoard(pieces){
  $('#tablero tr td').empty();
  positions = splitChainTextToArray(pieces);
  for(index = 0; index < positions.length ;index++){
    putInBoard(positions[index])
  }
}

function putInBoard(position){
  splitedPosition = position.split(":");
  point = splitedPosition[0] + splitedPosition[1];
  if(splitedPosition[2] == "NORTE"){
    $('#' + point).append('<div class="rojas"></div></div>');
  }
  if(splitedPosition[2] == "SUR"){
      $('#' + point).append('<div class="blancas"></div></div>');
  }
}

function setBoartProperties() {
    $('#tablero tr td div').addClass("draggable");
    $('#tablero tr td div').addClass("sortable");
    $('.sortable').sortable({revert: true});
    $( "tr, td" ).disableSelection();
}

function addAction(){
    var action = "";
    $('.draggable').draggable({
        connectToSortable: ".sortable",
        revert: "invalid",
        start: function(){
            parentPosition  = $(this).parent().attr('id');
            action += parentPosition;
            showAcceptedDroppable($(this));
        },
        stop: function(){
            parentPosition  = $(this).parent().attr('id');
            action += ":" + parentPosition;
            ws.send(action);
        }
    });
}

function splitChainTextToArray(pieces){
  return pieces.split(",",24);
}

function isValidPosition(positionX,positionY){
  return (positionX + positionY)%2 == 0;
}

function showAcceptedDroppable(selectedElement){
  parentId = selectedElement.parent().attr('id');
  positionX = parentId.split("")[0];
  positionY = parentId.split("")[1];
  
}

